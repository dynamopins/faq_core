from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from flask import Blueprint, jsonify

import requests

from rasa_core.channels.channel import UserMessage, OutputChannel
from rasa_core.channels.rest import HttpInputComponent

logger = logging.getLogger(__name__)


class RocketChatBot(OutputChannel):
    def __init__(self, url, access_token):
        self.access_token = access_token
        self.url = url
        self.messages = []

    def latest_output(self):
        if self.messages:
            return self.messages[-1]
        else:
            return None
    
    def send_text_message(self, recipient_id, message):
        
        url = self.url
        if self.access_token is not None:
            headers = {"Auth-token": self.access_token}
        else:
            headers = {}
        
        data = {'text': message}
        
        #requests.post(
        #    url,
        #    data,
        #    headers=headers
        #)

        self.messages.append({"recipient_id": recipient_id, "text": message})
        

class RocketChatInput(HttpInputComponent):
    def __init__(self, url, debug_mode):
        self.url = url
    
    def blueprint(self, on_new_message):
        from flask import request, Response
        slack_webhook = Blueprint('slack_webhook', __name__)
        
        @slack_webhook.route('/', methods=['GET'])
        def health():
            return jsonify({'status': 'ok'})
        
        @slack_webhook.route('/webhook', methods=['POST'])
        def event():
            payload = request.json
            channel_id = payload.get("channel_id")
            is_bot = payload.get("bot")
            user_id = payload.get("user_id")
            # url = payload.get("siteUrl", None)
            # token = payload.get("token", None)
            text = payload.get("text")
            # TODO Get the userId and pass it to on_new_message
            
            if not is_bot:
                output_channel = RocketChatBot(self.url, channel_id)
                on_new_message(UserMessage(text, output_channel, sender_id=user_id))
                print(output_channel.latest_output())
                return jsonify(output_channel.latest_output()), 200
            else:
                return "success"
        
        return slack_webhook
