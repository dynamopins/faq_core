from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import logging

from rasa_core.agent import Agent
from rasa_core.policies import FallbackPolicy
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy

if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    
    model_path = './models/dialogue'
    fallback = FallbackPolicy(fallback_action_name="utter_fallback",
                              core_threshold=0.5,
                              nlu_threshold=0.5)
    agent = Agent('domain.yml', policies=[MemoizationPolicy(), KerasPolicy(), fallback])

    training_data = agent.load_data('./data/stories.md')
    
    agent.train(
        training_data,
        augmentation_factor=50,
        epochs=70,
        batch_size=10,
        validation_split=0.2)
    
    agent.persist(model_path)
